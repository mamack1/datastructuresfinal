from config import db


# Class Disc is model that builds a disc object, it ensures that no disc object can have the same description by setting the unique argument to True
class Disc(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    disc = db.Column(db.String(80), unique=False, nullable=False)
    brand = db.Column(db.String(80), unique=False, nullable=False)
    color = db.Column(db.String(80), unique=False, nullable=False)
    weight = db.Column(db.String(80), unique=False, nullable=False)
    flight_numbers = db.Column(db.String(80), unique=False, nullable=False)
    description = db.Column(db.String(250), unique=True, nullable=False)

    # Here the information from our Disc object is converted to JSON by returning the object parameters as a dictionary, one of our main data structures
    def to_json(self):
        return {
            "id": self.id,
            "disc": self.disc,
            "brand": self.brand,
            "color": self.color,
            "weight": self.weight,
            "flightNumbers": self.flight_numbers,
            "description": self.description,
        }
