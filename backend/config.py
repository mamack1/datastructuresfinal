# Importing modules from flask:
# Flask is used to build the web app
# SQLAlchemy is used to interact with the database using python and to manage the database connection
# CORS is used to protect the access to the domain
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

# Initialize the app and enable CORS
app = Flask(__name__)
CORS(app)

# Configure database
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///disctracker.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

# Initialize database
db = SQLAlchemy(app)
