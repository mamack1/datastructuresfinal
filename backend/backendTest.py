import unittest
import requests
from models import Disc
from config import app, db


class TestDiscTracker(unittest.TestCase):
    BASE_URL = "http://localhost:5173"

    tilt_data = {
        "disc": "Tilt",
        "brand": "Discmania",
        "color": "Pink",
        "weight": "180g",
        "flightNumbers": "6-4-4-0",
        "description": "Utility Disc, crazy turn",
    }

    buzz_data = {
        "disc": "Buzz",
        "brand": "Discmania",
        "color": "Blue",
        "weight": "175g",
        "flightNumbers": "6-2-0-2",
        "description": "Laser Beam, slightly understable",
    }

    def test_get_discs(self):
        url = f"{self.BASE_URL}/discs"

        response = requests.get(url)
        self.assertEqual(response.status_code, 200)
        print("test 1 complete")

    def test_add_discs(self):

        url = f"{self.BASE_URL}/create_disc"

        print(f"Sending POST request to: {url}")

        response = requests.post(url, json=self.tilt_data)
        self.assertEqual(response.status_code, 201)
        print("test 2 complete")

    # add_tilt_response = requests.post(self.POST_URL, json=tilt_data)
    # self.assertEqual(add_tilt_response.status_code, 201)
    # print("Tilt disc added successfully")

    # add_buzz_response = requests.post(self.POST_URL, json=buzz_data)
    # self.assertEqual(add_buzz_response.status_code, 201)
    # print("Buzz disc added successfully")


if __name__ == "__main__":
    unittest.main()
