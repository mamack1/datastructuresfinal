import pytest
from config import app, db


@pytest.fixture
def test_client():
    with app.app_context():
        db.create_all()  # Create tables (if not already created)
        yield app.test_client()  # Yield the test client
        db.drop_all()  # Clean up the database after each test
