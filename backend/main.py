from flask import request, jsonify
from config import app, db
from models import Disc

# main.py file handles routes to the domain and their functions


# /discs is a get request that accesses the database and returns all of the discs in the database
@app.route("/discs", methods=["GET"])
def get_discs():
    discs = Disc.query.all()
    json_discs = list(map(lambda x: x.to_json(), discs))
    return jsonify({"discs": json_discs})


# /create_disc is a post request that creates a new Disc object and adds it to the database if the requirements are met, otherwise informs user on what they are missing
@app.route("/create_disc", methods=["POST"])
def create_disc():
    disc = request.json.get("disc")
    brand = request.json.get("brand")
    color = request.json.get("color")
    weight = request.json.get("weight")
    flight_numbers = request.json.get("flightNumbers")
    description = request.json.get("description")

    if not (disc or brand or color or weight or flight_numbers or description):
        return (
            jsonify(
                {
                    "message": "Please include something for each section, if any unknown, enter 'N/A'"
                }
            ),
            400,
        )

    new_disc = Disc(
        disc=disc,
        brand=brand,
        color=color,
        weight=weight,
        flight_numbers=flight_numbers,
        description=description,
    )
    try:
        db.session.add(new_disc)
        db.session.commit()
    except Exception as e:
        return jsonify({"message": str(e)}), 400

    return jsonify({"message": "Disc Added to Inventory!"}), 201


# /update_disc is a patch request that acceses a disc in the database that has the same id passed to the route
@app.route("/update_disc/<int:disc_id>", methods=["PATCH"])
def update_disc(disc_id):
    disc = Disc.query.get(disc_id)

    if not disc:
        return jsonify({"message": "Disc not found"}), 404

    data = request.json
    disc.disc = data.get("disc", disc.disc)
    disc.brand = data.get("brand", disc.brand)
    disc.color = data.get("color", disc.color)
    disc.weight = data.get("weight", disc.weight)
    disc.flight_numbers = data.get("flightNumbers", disc.flight_numbers)
    disc.description = data.get("description", disc.description)

    db.session.commit()

    return jsonify({"message": "Updated Disc Information"}), 200


# /delete_disc is a delete request that deletes the disc matching the passed id
@app.route("/delete_disc/<int:disc_id>", methods=["DELETE"])
def delete_disc(disc_id):
    disc = Disc.query.get(disc_id)

    if not disc:
        return jsonify({"message": "Disc not found"}), 404

    db.session.delete(disc)
    db.session.commit()

    return jsonify({"message": "Disc Deleted"}), 200


# The driver is responsible for running the server
if __name__ == "__main__":
    with app.app_context():
        db.create_all()

    app.run(debug=True)
