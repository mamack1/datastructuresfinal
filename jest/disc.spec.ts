const { Disc } = require("../frontend/src/App.tsx");

const disc: typeof Disc = {
	id: 1,
	disc: "Example Disc",
	brand: "Example Brand",
	color: "Red",
	weight: "175g",
	flightNumbers: "7, 5, -1, 1",
	description: "An example disc description",
};

test("should be able to create new Disc object", () => {
	expect(disc).toHaveProperty("id");
});
