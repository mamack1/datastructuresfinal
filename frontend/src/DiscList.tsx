import classes from "./DiscList.module.css";
import { FC } from "react";
import { Disc } from "./App.tsx";

// Sets props for the DiscList
interface DiscListProps {
	discs: Disc[];
	updateDisc: (disc: Disc) => void;
	updateCallback: () => void;
}

// the DiscList component is responsible for displaying disc information as well as accessing the update and delete functions
const DiscList: FC<DiscListProps> = ({ discs, updateDisc, updateCallback }) => {
	// onDelete is an async function that is passed the disc id, it then calls the /delete_disc endpoint
	const onDelete = async (id: number) => {
		try {
			const options = {
				method: "DELETE",
			};
			const response = await fetch(
				`http://127.0.0.1:5000/delete_disc/${id}`,
				options
			);
			if (response.status === 200) {
				updateCallback();
			} else {
				console.error("Failed to Delete Disc");
			}
		} catch (e) {
			alert(e);
		}
	};

	// this renders the DiscList componenet
	return (
		<div>
			<h1>Disc Tracker</h1>
			<table className={classes["discTable"]}>
				<thead>
					<tr className={classes["tableRows"]}>
						<th>Disc Name</th>
						<th>Brand</th>
						<th>Color</th>
						<th>Weight</th>
						<th>Flight Numbers</th>
						<th>Description</th>
						<th>Update/Delete</th>
					</tr>
				</thead>
				<tbody className={classes["tableBody"]}>
					{discs.map((disc) => (
						<tr key={disc.id}>
							<td>{disc.disc}</td>
							<td>{disc.brand}</td>
							<td>{disc.color}</td>
							<td>{disc.weight}</td>
							<td>{disc.flightNumbers}</td>
							<td>{disc.description}</td>
							<td>
								<button onClick={() => updateDisc(disc)}>Update</button>
								<button onClick={() => onDelete(disc.id)}>Delete</button>
							</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
};

export default DiscList;
