import { useState, useEffect, SetStateAction } from "react";
import DiscList from "./DiscList";
import "./App.css";
import DiscForm from "./DiscForm";

// defines Disc object
interface Disc {
	id: number;
	disc: string;
	brand: string;
	color: string;
	weight: string;
	flightNumbers: string;
	description: string;
}

// App is the main component for the frontend of our web app
function App() {
	// Here we use state to manage displaying the disc objects, and also opening and closing the modal used for adding and modifying discs
	const [discs, setDiscs] = useState<Disc[]>([]);
	const [isModalOpen, setIsModalOpen] = useState(false);
	const [currentDisc, setCurrentDisc] = useState({});

	// this is a hook that calls fetchDisc
	useEffect(() => {
		fetchDiscs();
	}, []);

	// fetchDiscs is an async function that calls the /discs endpoint from the backend, it then updates the discs state to display the disc objects
	const fetchDiscs = async () => {
		const response = await fetch("http://127.0.0.1:5000/discs");
		const data = await response.json();
		setDiscs(data.discs);
		console.log(data.discs);
	};

	// this function closes the modal
	const closeModal = () => {
		setIsModalOpen(false);
		setCurrentDisc({});
	};

	// this function opens the modal used to create a new disc
	const openCreateModal = () => {
		if (!isModalOpen) setIsModalOpen(true);
	};

	// this function opens the modal used to edit a disc
	const openEditModal = (disc: SetStateAction<{}>) => {
		if (isModalOpen) return;
		setCurrentDisc(disc);
		setIsModalOpen(true);
	};

	// this function closes the modal and calls the fetchdisc function, this results in updating the web page display to show the newly made updates
	const onUpdateDisc = () => {
		closeModal();
		fetchDiscs();
	};

	// sortDiscs is a bubble sort function that sorts the discs alphabetically
	const sortDiscs = () => {
		// Had to set sortedDiscs to a let so that I could reverse it before returning it
		let sortedDiscs = [...discs];
		const numDiscs = sortedDiscs.length;

		for (let i = 0; i < numDiscs; i++) {
			for (let j = 0; j < numDiscs; j++) {
				if (!sortedDiscs[j] || !sortedDiscs[j + 1]) continue;

				const discX = sortedDiscs[j].disc.toLowerCase();
				const discY = sortedDiscs[j + 1].disc.toLowerCase();

				if (discX < discY) {
					const z = sortedDiscs[j];
					sortedDiscs[j] = sortedDiscs[j + 1];
					sortedDiscs[j + 1] = z;
				}
			}
		}
		sortedDiscs = sortedDiscs.reverse();
		setDiscs(sortedDiscs);
	};

	// This is where the app is rendered
	return (
		<>
			{/* Disc List is displayed first, this contains the table for the disc information and options for modifying the objects */}
			<DiscList
				discs={discs}
				updateDisc={openEditModal}
				updateCallback={onUpdateDisc}
			/>{" "}
			<button onClick={sortDiscs}>Sort Discs By Disc Name</button>
			<button onClick={openCreateModal}>Create New Disc</button>
			{/* The section of code below handles the modal, calling the DiscForm when opened */}
			{isModalOpen && (
				<div className="modal">
					<div className="modal-content">
						<span className="close" onClick={closeModal}>
							exit
						</span>
						<DiscForm
							existingDisc={currentDisc}
							updateCallback={onUpdateDisc}
						/>
					</div>
				</div>
			)}
		</>
	);
}

export type { Disc };
export default App;
