import { useState } from "react";
import classes from "./DiscForm.module.css";
import { Disc } from "./App.tsx";

interface DiscFormProps {
	existingDisc?: Disc;
	updateCallback: () => void;
}

// DiscForm is a component that creates a form, the form is used to both add a new disc to the database and to update a disc in the database
const DiscForm = ({
	existingDisc = {} as Disc,
	updateCallback,
}: DiscFormProps) => {
	// useState is used to update the disc information that is sent to the database, it uses || to set the information in the form to either the information of the existing disc if it is being updated or to an empty string if it is creating a new disc
	const [disc, setDisc] = useState(existingDisc.disc || "");
	const [brand, setBrand] = useState(existingDisc.brand || "");
	const [color, setColor] = useState(existingDisc.color || "");
	const [weight, setWeight] = useState(existingDisc.weight || "");
	const [flightNumbers, setFlightNumbers] = useState(
		existingDisc.flightNumbers || ""
	);
	const [description, setDescription] = useState(
		existingDisc.description || ""
	);

	// updating is used to determine if we are updating hte disc or adding a new one, it checks if the length of the existingDisc object is 0. If it is 0, we are not updating, so we are creating a new disdc and vice versa.
	const updating = Object.entries(existingDisc).length !== 0;

	// onSubmit is the function called when we attempt to submit the form and access our database.
	const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();

		const data = {
			disc,
			brand,
			color,
			weight,
			flightNumbers,
			description,
		};

		// here updating is called, it is used to determine which endpoint is being called
		const url =
			"http://127.0.0.1:5000/" +
			(updating ? `update_disc/${existingDisc.id}` : "create_disc");

		// the options for the possible requests are set based on the updating call
		const options = {
			method: updating ? "PATCH" : "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify(data),
		};

		// the request is sent using the url, returning an alert with the error message if there was an error and calling updateCallback if succesfull
		const response = await fetch(url, options);
		if (response.status !== 201 && response.status !== 200) {
			const data = await response.json();
			alert(data.message);
		} else {
			updateCallback();
		}
	};

	// this renders the DiscForm component
	return (
		<form className={classes["discForm"]} onSubmit={onSubmit}>
			<div>
				<label htmlFor="disc">Disc:</label>
				<input
					type="text"
					id="disc"
					value={disc}
					onChange={(e) => setDisc(e.target.value)}
				/>
			</div>
			<div>
				<label htmlFor="brand">Brand:</label>
				<input
					type="text"
					id="brand"
					value={brand}
					onChange={(e) => setBrand(e.target.value)}
				/>
			</div>
			<div>
				<label htmlFor="color">Color:</label>
				<input
					type="text"
					id="color"
					value={color}
					onChange={(e) => setColor(e.target.value)}
				/>
			</div>
			<div>
				<label htmlFor="weight">Weight:</label>
				<input
					type="text"
					id="weight"
					value={weight}
					onChange={(e) => setWeight(e.target.value)}
				/>
			</div>
			<div>
				<label htmlFor="flightNumbers">Flight Numbers:</label>
				<input
					type="text"
					id="flightNumbers"
					value={flightNumbers}
					onChange={(e) => setFlightNumbers(e.target.value)}
				/>
			</div>
			<div>
				<label htmlFor="description">Description:</label>
				<input
					type="text"
					id="description"
					value={description}
					onChange={(e) => setDescription(e.target.value)}
				/>
			</div>
			<button type="submit">
				{updating ? "Update Disc Info" : "Add Disc"}
			</button>
		</form>
	);
};

export default DiscForm;
